# Ansible Collection - fsetit.general

Documentation for the collection.

## Included plugins

### fs-etit.general.gopass

## Included roles

### fs-etit.general.hostname

### fs-etit.general.hostsfile

### fs-etit.general.packagemanager

### fs-etit.general.packages

### fs-etit.general.repositories

### fs-etit.general.rootpassword

### fs-etit.general.unattended_upgrades
