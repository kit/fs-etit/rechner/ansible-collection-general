# fsetit.general.unattended_upgrades

Install upgrades, but not attend them

##  Optional variable

`unattended_upgrades_postfix_enabled`
Set this variable to false, if you do not want to install postfix as a dependency
