============================
fsetit.general Release Notes
============================

.. contents:: Topics

v2.1.0
======

Release Summary
---------------

Added role readmes for release on galaxy

v2.0.4
======

Release Summary
---------------

First release to galaxy.ansible.com
