import itertools
import os.path
import random
import secrets
import string
import subprocess
import time

from ansible.errors import AnsibleError, AnsibleLookupError
from ansible.module_utils.common.text.converters import to_bytes, to_text
from ansible.plugins.lookup import LookupBase
from ansible.utils.display import Display

DOCUMENTATION = """
    lookup: gopass
    author: Dominik Rimpf <dev@drimpf.de>
    short_description: read the value of gopass secrets
    description:
        - Allows you to query the values of gopass secrets available for the current user.
        - uses gopass: https://github.com/gopasspw/gopass
    options:
      _terms:
        description: path of secret to read
        required: True
      create:
        description: Flag to enable password generation
        required: False
        type: boolean
        default: False
      secret:
        description: secret to set if it does not exist instead of generating a random one
        required: False
        type: string
        default: ""
      password_type:
        description: Types of passwords to be generated. Choose one of "password" (classic) or "passphrase" (xkcd-style passwords)
        required: False
        type: string
        default: passphrase
        ini:
          - section: gopass
            key: password_type 
      wordfile:
        description: Path of the wordfile to use for passphrase generation
        required: False
        type: path
        default: /usr/share/dict/words
        ini:
          - section: gopass
            key: wordfile
      passphrase_word_min_length:
        description: Minimal length of the words used for passphrase
        required: False
        type: integer
        default: 5
        ini:
          - section: gopass
            key: passphrase_word_min_length
      passphrase_length:
        description: Number of words to be used for passphrase generation
        required: False
        type: integer
        default: 4
        ini:
          - section: gopass
            key: passphrase_length
      passphrase_separators:
        description: Seperator to be used for passphrase generation
        required: False
        type: list
        default: -,_,=,+
        ini:
          - section: gopass
            key: passphrase_separators
      passphrase_capitalize:
        description: Flag to enable capitalization words in Passphrase
        required: False
        type: boolean
        default: True
        ini:
          - section: gopass
            key: passphrase_capitalize
      passphrase_include_number:
        description: Flag to enable addition of a number to the passphrase
        required: False
        type: boolean
        default: True
        ini:
          - section: gopass
            key: passphrase_include_number
      passphrase_number_max:
        description: Upper bound (exclusive) for number
        required: False
        type: integer
        default: 1000
        ini:
          - section: gopass
            key: passphrase_number_max
      password_length:
        description: Length of generated passwords
        required: False
        ini:
          - section: gopass
            key: password_length
        type: integer
        default: 24
      password_use_symbols:
        description: Generate password with punctuation symbols
        required: False
        type: boolean
        default: False
        ini:
         - section: gopass
           key: password_use_symbols
      mount_path:
        description: common mount path to all ansible secrets
        required: False
        type: string
        default: ""
        ini:
          - section: gopass
            key: mount_path
"""

display = Display()


class LookupModule(LookupBase):
    password = None
    path = None
    env = None
    words = None
    generate = None

    def check_password(self):
        try:
            output = subprocess.check_output(['gopass', 'show', '--password', self.path], env=self.env,
                                             stderr=subprocess.STDOUT)
            self.password = to_text(output)
            return True
        except subprocess.CalledProcessError as e:
            # 'entry is not in the password store' is the expected error if a password wasn't found
            if 'entry is not in the password store' not in to_text(e.output, errors='surrogate_or_strict'):
                raise AnsibleError(e)

        return False

    def generate_password(self):
        length = self.get_option("password_length")
        use_symbols = self.get_option("password_use_symbols")
        alphabet = string.ascii_letters + string.digits
        if use_symbols:
            alphabet += string.punctuation
        self.password = ''.join(secrets.choice(alphabet) for i in range(length))
        return

    def generate_passphrase(self):
        length = self.get_option('passphrase_length')
        separators = self.get_option('passphrase_separators')

        words = [secrets.choice(self.words).capitalize() if self.get_option('passphrase_capitalize')
                 else secrets.choice(self.words) for i in range(length)]
        seps = [secrets.choice(separators) for i in range(length - 1)]

        if self.get_option('passphrase_include_number'):
            number_pos = secrets.randbelow(length)
            number = secrets.randbelow(self.get_option('passphrase_number_max'))
            words[number_pos] = ''.join(random.sample([str(number), words[number_pos]], k=2))

        password = [item for item in itertools.chain.from_iterable(itertools.zip_longest(words, seps)) if
                    item is not None]
        self.password = ''.join(password)

    def insert(self):
        now = time.strftime('%Y-%m-%d %H:%M:%S')
        content = f'{self.password}\ncomment: generated by ansible on {now}\n'
        try:
            subprocess.check_output(['gopass', 'insert', '--multiline', self.path],
                                    input=to_bytes(content, errors='surrogate_or_strict'), env=self.env,
                                    stderr=subprocess.STDOUT)
        except subprocess.CalledProcessError as e:
            raise AnsibleError(e)

    def setup(self):
        self.env = os.environ.copy()
        self.env['LANGUAGE'] = 'C'

        type = self.get_option('password_type')

        if type == 'passphrase':
            wordfile_path = self.get_option('wordfile')
            with open(wordfile_path, 'r') as wordfile:
                words = wordfile.read().split('\n')

            min_length = self.get_option('passphrase_word_min_length')
            self.words = [word for word in words if len(word) >= min_length]

            self.generate = self.generate_passphrase
        elif type == 'password':
            self.generate = self.generate_password

    def run(self, terms, variables=None, **kwargs):
        self.set_options(direct=kwargs)
        self.setup()

        self.path = os.path.join(self.get_option('mount_path'), terms[0])
        if self.check_password():  # if password exists
            pass
        elif self.get_option('create'):  # if not exists, and create is true
            if secret := self.get_option('secret'):
                self.password = secret
            else:
                self.generate()
            if not variables["ansible_check_mode"]:
                self.insert()
        else:  # if not exists and create is false
            raise AnsibleLookupError(f'Secret {self.path} not found, use create=True to create not existing secrets.')
        return [self.password]
